AyudAR
======

AyudAR consiste en un sistema de hardware y software que permite manejar una computadora a personas con difucultades motrices. 

Componentes del sistema
-----------------------

AyudAR está formado por un sensor (acelerómetro - giroscopio - magnetómetro), una placa microcontroladora ARDUINO y una computadora. Dos piezas de software son utilizadas, por un lado el firmware que corre en ARDUINO (C/C++), encargado de leer la información del sensor y enviarla por el puerto serie (USB) a la PC y, por el otro, el que lo recibe en la computadora (Python), procesa y lleva a cabo las acciones de movimiento del mouse o tipeo de carateres.

.. figure:: img/esquema_general.png
    :width: 500 px
    
    Esquema general del sistema

Sensor
''''''

Se utilizó el módulo MPU9250 (basado en el sensor InvenSense MPU9250) que contiene en la misma placa un giroscopo de 3 ejes, un acelerómetro de 3 ejes y un magnetometro de 3 ejes. 


Microcontrolador
''''''''''''''''

Se utilizó un microntrolador ARDUINO UNO rev3...

Computadora
'''''''''''

El sistema operativo utilizado en la PC es un GNU/Linux Debian...

Firmware y software
-------------------

continuará...
